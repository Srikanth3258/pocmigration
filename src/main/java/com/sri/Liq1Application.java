package com.sri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Liq1Application {

	public static void main(String[] args) {
		SpringApplication.run(Liq1Application.class, args);
	}

}
